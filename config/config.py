import pathlib

import model_package

PACKAGE_ROOT = pathlib.Path(model_package.__file__).resolve().parent

# Model Features
FEATURES = ['Pclass_third', 'Pclass_first', 'Pclass_second', 'Age', 'Fare',
			'Embarked_S', 'Embarked_C', 'Embarked_Q', 'Person_male',
			'Person_female', 'Person_Child', 'Family', 'Status_Mr', 'Status_Mrs',
			'Status_Miss', 'Status_Other']